class Request {
	int targetFloor;

	public Request(int targetFloor) {
		this.targetFloor = targetFloor;
	}

	public int getTargetFloor() {
		return targetFloor;
	}
}