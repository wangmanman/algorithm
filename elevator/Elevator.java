class Elevator {
	public static Elevator INSTANCE;

	private int currentFloor;

	private Elevator() {
	}

	public static Elevator getInstance() {
		if (INSTANCE == null) {
			synchronized (Elevator.class) {
				if (INSTANCE == null) {
					INSTANCE = new Elevator();
				}
			}
		}
		return INSTANCE;
	}

	public int getCurrentFloor() {
		return currentFloor;
	}

	public void moveTo(int targetFloor) {
		currentFloor = targetFloor;
	}

	public void buttonPressed(int targetFloor) {
		Request request = new Request(targetFloor);
		RequestProcessCentre.getInstance().addRequest(request);
	}
}