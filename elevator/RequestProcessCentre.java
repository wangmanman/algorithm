import java.util.ArrayList;
import java.util.List;

class RequestProcessCentre implements Runnable {
	
	private static RequestProcessCentre INSTANCE;

	public List<Request> queue;

	private RequestProcessCentre() {
		queue = new ArrayList<Request>();
	}

	public static RequestProcessCentre getInstance() {
		if (INSTANCE == null) {
			 synchronized (RequestProcessCentre.class) {
			 	if (INSTANCE == null) {
			 		INSTANCE = new RequestProcessCentre();
			 	}
			 }
		}
		return INSTANCE;
	}

	public void addRequest(Request request) {
		if (request != null) {
			queue.add(request);
		}
	}

	public void removeRequest(Request request) {
		if (request != null) {
			queue.remove(request);
		}
	}

	public Request getNextRequest() {
		int currentFloor = Elevator.getInstance().getCurrentFloor();
		int shortest = Integer.MAX_VALUE;
		Request shortestRequest = null;
		for (Request request: queue) {
			if (Math.abs(currentFloor - request.getTargetFloor()) < shortest) {
				shortest = Math.abs(currentFloor - request.getTargetFloor());
				shortestRequest = request;
			}
		}
		return shortestRequest;
	}
	
	public void processRequest() {
		Request request = getNextRequest();
		Elevator.getInstance().moveTo(request.getTargetFloor());
	}

	@Override
	public void run() {
		while (true) {
			getNextRequest();
		}
	}
}