import java.util.HashMap;
import java.util.Map;

public class TwoSumSolutions {

	public int[] twoSumSolution_1(int[] nums, int target) {
		for (int i = 0; i < nums.length; i++) {
			for (int j = i + 1; j < nums.length; i++) {
				if (nums[i] + nums[j] == target) {
					return new int[] {i, j};
				}
			}
		}
		throw new IllegalArgumentException("No solution");
	}
	
	public int[] twoSumSolution_2(int[] nums, int target) {
		Map<Integer, Integer> hashMap = new HashMap<Integer, Integer>();
		for (int i = 0; i < nums.length; i++) {
			hashMap.put(nums[i], i);
		}
		for (int i = 0; i < nums.length; i++) {
			int complement = target - nums[i];
			if (hashMap.containsKey(complement) && (complement != i)) {
				return new int[] {i, hashMap.get(complement)};
			}
		}
		throw new IllegalArgumentException("No solution");
	}
	
	public int[] twoSumSolution_3(int[] nums, int target) {
		Map<Integer, Integer> hashMap = new HashMap<Integer, Integer>();
		for (int i = 0; i < nums.length; i++) {
			if (hashMap.containsKey(target - nums[i])) {
				return new int[] {hashMap.get(target - nums[i]), i};
			}
			else {
				hashMap.put(nums[i], i);
			}
		}
		throw new IllegalArgumentException("No Solution");
	}
}
