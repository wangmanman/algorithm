package addTwoNumbers;

public class AddTwoNumbersSolutions {

	 public class ListNode {
		 int val;
		 ListNode next;
		 ListNode(int x) { val = x; }
	 }
	
	public ListNode addTwoNumbersSolution_1(ListNode l1, ListNode l2) {
        ListNode head = new ListNode(0);
        ListNode headPointer = head;
        ListNode l1Pointer = l1;
        ListNode l2Pointer = l2;
        
        int carry = 0;
        
		while (l1Pointer != null || l2Pointer != null) {
			int sum = carry;
			if (l1Pointer != null) {
				sum = sum + l1Pointer.val;
			}
			if (l2Pointer != null) {
				sum = sum + l2Pointer.val;
			}
			carry = sum / 10;
			headPointer.next = new ListNode(sum % 10);
			headPointer = headPointer.next;
			l1Pointer = l1Pointer.next;
			l2Pointer = l2Pointer.next;
		}
		if (carry > 0) {
			headPointer.next = new ListNode(1);
		}
		return head.next;
    }
}
